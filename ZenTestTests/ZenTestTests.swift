//
//  ZenTestTests.swift
//  ZenTestTests
//
//  Created by AngryGOsha on 01.07.2021.
//

import XCTest
@testable import ZenTest

class ZenTestTests: XCTestCase {

//    override func setUpWithError() throws {
//        // Put setup code here. This method is called before the invocation of each test method in the class.
//    }
//
//    override func tearDownWithError() throws {
//        // Put teardown code here. This method is called after the invocation of each test method in the class.
//    }
//
//    func testExample() throws {
//        // This is an example of a functional test case.
//        // Use XCTAssert and related functions to verify your tests produce the correct results.
//    }
//
//    func testPerformanceExample() throws {
//        // This is an example of a performance test case.
//        self.measure {
//            // Put the code you want to measure the time of here.
//        }
//    }
    
    func testListVCInit() {
        
        // Given
        let vc = NewPersonViewController()
        
        vc.addUser(person: Mocks.persons[1])
        
        // When
        let person = vc.getUser()
        
        // Then
        XCTAssertTrue(person is Worker)
        XCTAssertTrue(person is Leader)
    }

}
