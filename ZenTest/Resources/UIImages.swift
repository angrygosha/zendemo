//
//  UIImages.swift
//  ZenTest
//
//  Created by AngryGOsha on 02.07.2021.
//

import Foundation
import UIKit

extension UIImage {
    
    struct logo {
        static let splashIcon = { #imageLiteral(resourceName: "SplashScreen") }()
    }
    
    struct tabBarImages {
        static let list = { #imageLiteral(resourceName: "ListIcon") }()
        static let gallery = { #imageLiteral(resourceName: "GalleryIcon") }()
        static let service = { #imageLiteral(resourceName: "ServiceIcon") }()
    }
    
    struct mocks {
        static let avatarMock = { #imageLiteral(resourceName: "Avatar") }()
    }
    
}
