//
//  Person.swift
//  ZenTest
//
//  Created by AngryGOsha on 02.07.2021.
//

import Foundation
import UIKit

enum BookerType: String {
    case salary = "Начисление ЗП"
    case materials = "Учет материалов"
}

enum PostType: String {
    case leader = "Руководитель"
    case worker = "Сотрудник"
    case booker = "Бухгалтер"
}

protocol Person {
    var avatar: UIImage? { get set }
    var family: String { get set }
    var name: String { get set }
    var secondName: String { get set }
    var salary: Double { get set }
    var postType: String { get set }
}

extension Person {
    var fullNameForList: String {
        get {
            return "\(family) \(name) \(secondName)"
        }
    }
    
    var fullNameForDetailed: String {
        get {
            return "\(family)\n\(name) \(secondName)"
        }
    }
}

struct Leader: Person {
    var avatar: UIImage?
    
    var family: String
    
    var name: String
    
    var secondName: String
    
    var salary: Double
    
    var postType: String = PostType.leader.rawValue
    
    let hours: String
}

struct Worker: Person {
    var avatar: UIImage?
    
    var family: String
    
    var name: String
    
    var secondName: String
    
    var salary: Double
    
    var postType: String = PostType.worker.rawValue
    
    var armId: Int
    
    var dinner: String
}

struct Booker: Person {
    var avatar: UIImage?
    
    var family: String
    
    var name: String
    
    var secondName: String
    
    var salary: Double
    
    var postType: String = PostType.booker.rawValue
    
    var armId: Int
    
    var dinner: String
    
    let type: BookerType
}



