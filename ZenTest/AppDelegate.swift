//
//  AppDelegate.swift
//  ZenTest
//
//  Created by AngryGOsha on 01.07.2021.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        setupAppearance()
        
        window = UIWindow()
        window?.rootViewController = SplashScreenView()
        window?.makeKeyAndVisible()
        
        //TODO: Потом раскоментить когда допилю всякие загрузки баз и тд чтоб сплеш был, а то пока только замедляет
//        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1) {
            let vc = MainTabBarViewController()
            vc.title = "asdasd"
            self.window?.rootViewController = vc
            self.window?.makeKeyAndVisible()
//        }
        
        return true
    }
    
    private func setupAppearance() {
        UITabBar.appearance().barTintColor = .white
        UITabBar.appearance().isTranslucent = false
        UITabBar.appearance().tintColor = .black

        UINavigationBar.appearance().barTintColor = .white
        UINavigationBar.appearance().tintColor = .black
        UINavigationBar.appearance().titleTextAttributes =  [NSAttributedString.Key.foregroundColor: UIColor.black]
    }

}

