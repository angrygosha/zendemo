//
//  NewPersonViewController.swift
//  ZenTest
//
//  Created by AngryGOsha on 05.07.2021.
//

import Foundation
import UIKit
import SnapKit

protocol testableProtocol {
    func getUser() -> Person?
    func addUser(person: Person)
}

final class NewPersonViewController: UIViewController {
    
    private var person: Person?
    
    let label: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.numberOfLines = 2
        return label
    }()
    
    let inputField: UITextField = {
        let input = UITextField()
        input.placeholder = "family"
        return input
        
    }()
    
    let imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        //TODO: вынести в константу или перенести в метод для округления
        imageView.layer.cornerRadius = 75
        imageView.clipsToBounds = true
        
        imageView.layer.borderWidth = 0.5
        imageView.layer.borderColor = UIColor.lightGray.cgColor
        return imageView
    }()
    
    let spacer = UIView()
    
    init(person: Person? = nil) {
        self.person = person
        if let person = person {
            imageView.image = person.avatar ?? UIImage.mocks.avatarMock
            label.text = person.fullNameForDetailed
        } else {
            imageView.image = .add
        }
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        guard (navigationController?.navigationBar) != nil else { return }
        
        //TODO: заменить иконку .actions на свою
        let rightButtonItem = UIBarButtonItem(image: nil, style: .done, target: self, action: #selector(saveButtonTapped))
        rightButtonItem.title = "[S]"
        navigationItem.rightBarButtonItem = rightButtonItem
    }
    
    override func viewDidLoad() {
        view.backgroundColor = .white
        
        self.navigationItem.title = person == nil ? "New person" : "Detailed info"
        
        addSubviews()
        setupConstraints()
    }
    
    private func addSubviews() {
        view.addSubview(imageView)
        view.addSubview(label)
        view.addSubview(inputField)
        view.addSubview(spacer)
    }
    
    private func setupConstraints() {
        imageView.snp.makeConstraints { make in
            //TODO: верх уходит под навбар, offset на 100 это костыль
            make.top.equalToSuperview().offset(100)
            make.centerX.equalToSuperview()
            //TODO: в константу
            make.width.height.equalTo(150)
        }
        
        label.snp.makeConstraints { make in
            //TODO: в константы
            make.top.equalTo(imageView.snp.bottom).offset(20)
            make.width.equalTo(200)
            make.centerX.equalToSuperview()
        }
        
        inputField.backgroundColor = .red
        
        inputField.snp.makeConstraints { make in
            make.top.equalTo(label.snp.bottom)
            make.left.trailing.equalToSuperview()
            make.centerX.equalToSuperview()
//            make.bottom.leading.trailing.equalToSuperview()
            
        }
        
        spacer.snp.makeConstraints { make in
            make.top.equalTo(inputField.snp.bottom)
            make.bottom.leading.trailing.equalToSuperview()
        }
    }
    
    @objc private func saveButtonTapped() {
        print("сохранение сотрудника")
    }
}

extension NewPersonViewController: testableProtocol {
    func addUser(person: Person) {
        self.person = person
    }
    
    func getUser() -> Person? {
        return person
    }
}
