//
//  PersonCell.swift
//  ZenTest
//
//  Created by AngryGOsha on 03.07.2021.
//

import Foundation
import UIKit
import SnapKit

private struct UIConstants {
    static let infoStackViewOffset: CGFloat = 10
    
    static let nameFont: UIFont = .systemFont(ofSize: 16, weight: .regular)
    static let postFont: UIFont = .systemFont(ofSize: 14, weight: .light)
}

final class PersonCell: UITableViewCell {
    
    private let avatarImage: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        //TODO: вынести в константу или перенести в метод для округления
        imageView.layer.cornerRadius = 25
        imageView.clipsToBounds = true
        
        imageView.layer.borderWidth = 0.5
        imageView.layer.borderColor = UIColor.lightGray.cgColor
        
        return imageView
    }()
    
    private let fullName: UILabel = {
        let label = UILabel()
        label.font = UIConstants.nameFont
        return label
    }()
    
    private let post: UILabel = {
        let label = UILabel()
        label.font = UIConstants.postFont
        return label
    }()
    
    private let mainStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        return stackView
    }()
    
    private let infoStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        return stackView
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func commonInit() {
        addSubViews()
        setupSubViews()
    }
    
    private func addSubViews() {
        
        addSubview(mainStackView)
        
        mainStackView.addSubview(avatarImage)
        mainStackView.addSubview(infoStackView)
        
        infoStackView.addSubview(fullName)
        infoStackView.addSubview(post)

    }
    
    private func setupSubViews() {
        
        mainStackView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }

        avatarImage.snp.makeConstraints { make in
            make.leading.top.equalToSuperview().offset(2)
            make.bottom.equalToSuperview().inset(2)
            make.width.equalTo(50)
        }
        
        infoStackView.snp.makeConstraints { make in
            make.leading.equalTo(avatarImage.snp.trailing).offset(2)
            make.top.bottom.trailing.equalToSuperview()
        }

        fullName.snp.makeConstraints { make in

        }

        post.snp.makeConstraints { make in
            make.top.equalTo(fullName.snp.bottom)
        }
    }
    
    func configure(person: Person) {
        
                                        //TODO: вот эту штуку надо пренести в модель
        avatarImage.image = person.avatar ?? UIImage.mocks.avatarMock
        fullName.text = person.fullNameForList
        post.text = person.postType
    }
    
}
