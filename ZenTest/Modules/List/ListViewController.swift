//
//  ListViewController.swift
//  ZenTest
//
//  Created by AngryGOsha on 02.07.2021.
//

import Foundation
import UIKit
import SnapKit

final class ListViewController: UIViewController, UITableViewDelegate {
    
    private let tableView: UITableView = {
        let tableView =  UITableView()
        tableView.layoutMargins = .zero
        tableView.separatorInset = .zero
        return tableView
    }()
    
    private var personsArray: [Person] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "List"
        
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(PersonCell.self, forCellReuseIdentifier: "cell")
        
        getPersons()
        
        commonInit()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        guard (navigationController?.navigationBar) != nil else { return }
        
        let rightButtonItem = UIBarButtonItem(image: .add, style: .done, target: self, action: #selector(addButtonTapped))
        navigationItem.rightBarButtonItem = rightButtonItem
    }
    
    private func commonInit() {
        view.backgroundColor = .white
        addSubViews()
        setupSubViews()
    }
    
    private func addSubViews() {
        view.addSubview(tableView)
    }
    
    private func setupSubViews() {
        tableView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
    
    @objc private func addButtonTapped() {
        self.navigationController?.pushViewController(NewPersonViewController(), animated: true)
    }
    
    //TODO: пренести в контроллер
    private func getPersons() {
        personsArray = Mocks.generatePersons(leaderCount: 5, workerCount: 73, bookerCount: 8)
    }
    
}

//MARK: - UITableViewDataSource

extension ListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return personsArray.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! PersonCell
    
        cell.configure(person: personsArray[indexPath.row])

        return cell
            
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath.row)
        
        self.navigationController?.pushViewController(NewPersonViewController(person: personsArray[indexPath.row]), animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        //TODO: перенести в константу
        return 54
    }

}
