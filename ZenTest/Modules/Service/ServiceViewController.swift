//
//  ServiceViewController.swift
//  ZenTest
//
//  Created by AngryGOsha on 02.07.2021.
//

import Foundation
import UIKit

final class ServiceViewController: UIViewController {
    let text: UILabel = {
        let label = UILabel()
        label.text = "ThirdScreen"
        return label
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Service"
        commonInit()
    }
    
    private func commonInit() {
        view.backgroundColor = .white
        addSubViews()
        setupSubViews()
    }
    
    private func addSubViews() {
        view.addSubview(text)
    }
    
    private func setupSubViews() {
        text.snp.makeConstraints { make in
            make.center.equalToSuperview()
        }
    }

}
