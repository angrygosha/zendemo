//
//  SplashScreenView.swift
//  ZenTest
//
//  Created by AngryGOsha on 02.07.2021.
//

import Foundation
import UIKit
import SnapKit

final class SplashScreenView: UIViewController {
    
    private let logoImage: UIImageView = {
        var imageView = UIImageView()
        imageView.image = UIImage.logo.splashIcon
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        commonInit()
    }
    
    private func commonInit() {
        view.backgroundColor = .white
        addSubViews()
        setupSubViews()
    }
    
    private func addSubViews() {
        view.addSubview(logoImage)
    }
    
    private func setupSubViews() {
        logoImage.snp.makeConstraints { make in
            make.center.equalToSuperview()
            make.width.equalToSuperview().multipliedBy(0.6)
        }
    }
}
