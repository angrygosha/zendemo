//
//  GalleryViewController.swift
//  ZenTest
//
//  Created by AngryGOsha on 02.07.2021.
//

import Foundation
import UIKit

final class GalleryViewController: UIViewController {
    let text: UILabel = {
        let label = UILabel()
        label.text = "SecondScreen"
        return label
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Gallery"
        commonInit()
    }
    
    private func commonInit() {
        view.backgroundColor = .white
        addSubViews()
        setupSubViews()
    }
    
    private func addSubViews() {
        view.addSubview(text)
    }
    
    private func setupSubViews() {
        text.snp.makeConstraints { make in
            make.center.equalToSuperview()
        }
    }

}
