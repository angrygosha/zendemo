//
//  MainTabBarViewController.swift
//  ZenTest
//
//  Created by AngryGOsha on 02.07.2021.
//

import Foundation
import UIKit
import SnapKit

final class MainTabBarViewController: UITabBarController {
    
    let listNavController = UINavigationController(rootViewController: ListViewController())
    let galleryNavController = UINavigationController(rootViewController: GalleryViewController())
    let serviceNavController = UINavigationController(rootViewController: ServiceViewController())
    
    override func viewDidLoad() {
        super.viewDidLoad()
        commonInit()
    }
    
    private func commonInit() {
        configureTabBarItems()
    }
    
    private func configureTabBarItems() {
        listNavController.tabBarItem = UITabBarItem(title: nil, image: UIImage.tabBarImages.list, tag: 0)

        galleryNavController.tabBarItem = UITabBarItem(title: nil, image: UIImage.tabBarImages.gallery, tag: 1)

        serviceNavController.tabBarItem = UITabBarItem(title: nil, image: UIImage.tabBarImages.service , tag: 2)

        viewControllers = [
            listNavController,
            galleryNavController,
            serviceNavController
        ]
    }

}
