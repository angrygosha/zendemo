//
//  Mocks.swift
//  ZenTest
//
//  Created by AngryGOsha on 02.07.2021.
//

import Foundation
class Mocks {
    static let persons: [Person] = [
        Leader(family: "lF1", name: "lN1", secondName: "lS1", salary: 1, hours: "chasi"),
        Worker(family: "wF1", name: "wN1", secondName: "wS1", salary: 1, armId: 1, dinner: "dinner"),
        Booker(family: "bF1", name: "bN1", secondName: "bS1", salary: 1, armId: 1, dinner: "dinner", type: .materials)
    ]
    
    static func generatePersons(leaderCount: Int, workerCount: Int, bookerCount: Int) -> [Person] {
        var array: [Person] = []
        
        for i in 0...leaderCount {
            let leader = Leader(family: "lFamily\(i)", name: "lName\(i)", secondName: "lSecondName\(i)", salary: 1, hours: "chasi")
            array.append(leader)
        }
        
        for i in 0...workerCount {
            let worker = Worker(family: "wFamily\(i)", name: "wName\(i)", secondName: "wSecondName\(i)", salary: 1, armId: 1, dinner: "dinner")
            array.append(worker)
        }
        
        for i in 0...bookerCount {
            let booker = Booker(family: "bFamily\(i)", name: "bName\(i)", secondName: "bSecondName\(i)", salary: 1, armId: 1, dinner: "dinner", type: .materials)
            array.append(booker)
        }
        
        return array
    }
}
